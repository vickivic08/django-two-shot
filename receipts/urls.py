from django.contrib import admin
from django.urls import path
from receipts.views import list_receipt, create_receipt

urlpatterns = [
    path("", list_receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
]
